//
//  ViewController.swift
//  VideoPlayer
//
//  Created by M K on 2020/04/10.
//  Copyright © 2020 M K. All rights reserved.
//

import UIKit
import Photos   //写真や動画を扱うためのインターフェイス
import AVKit    //ビデオコンテンツを再生するためのインターフェイス


class VideoViewController: UIViewController ,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    // 選択された動画のurlを保持するための変数
    private var videoUrl: NSURL?
    
    // UIImagePickerController インタンスの作成
    // メディア周りの機能を簡単に扱えるようにするクラス
    let imagePickerController = UIImagePickerController()
    
    // AVPlayerViewController インタンスの作成
    // Video操作を簡単に行うためのクラス
    private let playerViewController = AVPlayerViewController()
    
    // ImageView
    @IBOutlet weak var thumbnailImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //imagePickerControllerの代わりにVideoUploaderViewControllerが役割を肩代わりしますよ！という宣言
        self.imagePickerController.delegate = self
        
        // MARK: allow access to camera roll
        self.confirmPhotoLibraryAuthenticationStatus()
    
    }
    
    // Play ボタンがタップされた時に呼び出される
    @IBAction func didTapPlayButton2(_ sender: Any) {
        // 選択された動画の絶対パスがオプショナル(nilの可能性がある)ので
        // guard(railsでいうunless)でパスがnilなら早期リターンにしてる
        
        guard let url = videoUrl?.absoluteURL else { return }
        playVideo(from: url)
    }
    
    //Selectボタンがタップされた時に呼び出される
    @IBAction func didTapSelectButton(_ sender: Any) {
        selectVideo()
    }
    
    
    
    private func confirmPhotoLibraryAuthenticationStatus() {
        //権限の現状確認(許可されているかどうか)
        if PHPhotoLibrary.authorizationStatus() != .authorized {
            //許可(authorized)されていない・ここで初回のアラートが出る
            PHPhotoLibrary.requestAuthorization { status in
                switch status {
                //もし状態(status)が、初回(notDetermined)もしくは拒否されている(denied)の場合
                case .notDetermined, .denied:
                    //許可しなおして欲しいので、設定アプリへの導線をおく
                    self.appearChangeStatusAlert()
                default:
                    break
                }
            }
        }
    }
    
    
    private func appearChangeStatusAlert() {
        // iOS13以降はメインスレッド以外でUI更新をしてクラッシュする
        // よって、下記の方法でキューに入れ込めばOK
        // https://qiita.com/rymiyamoto/items/7ace750172b84a2ff809
        DispatchQueue.main.sync{
            //フォトライブラリへのアクセスを許可していないユーザーに対して設定のし直しを促す。
            //タイトルとメッセージを設定しアラートモーダルを作成する
            let alert = UIAlertController(title: "Not authorized", message: "we need to access photo library to upload video", preferredStyle: .alert)
            //アラートには設定アプリを起動するアクションとキャンセルアクションを設置
            let settingAction = UIAlertAction(title: "setting", style: .default, handler: { (_) in
                guard let settingUrl = URL(string: UIApplication.openSettingsURLString) else { return }
                UIApplication.shared.open(settingUrl, options: [:], completionHandler: nil)
            })
            let closeAction = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
            //アラートに上記の２つのアクションを追加
            alert.addAction(settingAction)
            alert.addAction(closeAction)
            //アラートを表示させる
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    private func selectVideo() {
        //選択できるメディアは動画のみを指定
        self.imagePickerController.mediaTypes = ["public.movie"]
        //選択元はフォトライブラリ
        self.imagePickerController.sourceType = .photoLibrary
        //実際にimagePickerControllerを呼び出してフォトライブラリを開く
        self.present(self.imagePickerController, animated: true, completion: nil)
    }
    
    //imagePickerが閉じられる時に呼ばれる
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //キーを指定して選択された動画のパスを取得する
        let key = UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerMediaURL")
        videoUrl = info[key] as? NSURL
        //動画の絶対パスを元にサムネイルを生成(generateThumbnailFromVideoの実装は後述します。)
        //先ほど接続したthumbnailImageViewのimageにサムネイルをセット
        thumbnailImageView.image = generateThumbnailFromVideo((videoUrl?.absoluteURL)!)
        //サムネイルの縦横比を変えずに長い辺を画面サイズに合わせる設定
        thumbnailImageView.contentMode = .scaleAspectFit
        //imagePickerControllerを閉じる
        imagePickerController.dismiss(animated: true, completion: nil)
    }
    
    
    private func generateThumbnailFromVideo(_ url: URL) -> UIImage? {
        //以下の３行で縦動画から画像を取り出しても横向きの画像にならないようにしてる
        let asset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        //縦動画からサムネイルを切り出した時に正しい向きで表示させる設定
        imageGenerator.appliesPreferredTrackTransform = true
        //切り取るタイミングの指定
        var time = asset.duration
        time.value = min(time.value, 2)
        //サムネイルの生成
        do {
            let imageRef = try imageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch {
            return nil
        }
    }
    
    //動画再生を行う関数
    private func playVideo(from url: URL) {
        //プレイヤーに受けとったurlをセット
        let player = AVPlayer(url: url)
        //先ほど初期化したplayerViewControllerのプレイヤーに上記のプレイヤーをセット
        playerViewController.player = player
        //playerViewControllerの表示・再生
        self.present(playerViewController, animated: true) {
            print("playing video")
            self.playerViewController.player!.play()
        }
    }
    
    
    
    
    
    
}

